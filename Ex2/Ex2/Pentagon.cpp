#include "Pentagon.h"
#include "MathUtils.h"
#include "shapeexception.h"

Pentagon::Pentagon(std::string nam, std::string col, double side) : Shape(nam,col)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}
void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "side is " << getSide() << std::endl << "Area: " << CalArea() << std::endl;;

}
double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(_side);
}
void Pentagon::setSide(double side)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}
double Pentagon::getSide()
{
	return _side;
}
