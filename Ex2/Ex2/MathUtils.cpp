#include "MathUtils.h"

double MathUtils::CalPentagonArea(double w)
{
	return 1.72048 * w * w;
}
double MathUtils::CalHexagonArea(double w)
{
	return 2.59808 * w * w;
}
