#include "CharException.h"

const char* CharException::what() const
{
	return "Warning - Don't try to build more than one shape at once\n";
}