#include "Hexagon.h"
#include "shapeexception.h"
#include "MathUtils.h"

Hexagon::Hexagon(std::string nam, std::string col, double side) : Shape(nam, col)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}
void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "side is " << getSide() << std::endl << "Area: " << CalArea() << std::endl;;

}
double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(_side);
}
double Hexagon::getSide()
{
	return _side;
}
void Hexagon::setSide(double side)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}