#pragma once
class MathUtils
{
public:
	static double CalPentagonArea(double w);
	static double CalHexagonArea(double w);
};

