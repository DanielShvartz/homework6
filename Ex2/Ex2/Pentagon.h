#pragma once
#include "shape.h"
class Pentagon : public Shape
{
public:
	Pentagon(std::string nam, std::string col, double side);
	virtual void draw();
	virtual double CalArea();
	void setSide(double side);
	double getSide();

private:
	double _side;
};

