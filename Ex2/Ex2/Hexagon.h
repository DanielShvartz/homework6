#pragma once
#include "shape.h"

class Hexagon : public Shape
{
public:
	Hexagon(std::string nam, std::string col, double side);
	virtual void draw();
	virtual double CalArea();
	double getSide();
	void setSide(double side);
private:
	double _side;
};

