#include <iostream>
int add(int a, int b) 
{
	if (a == 8200 || b == 8200 || a + b == 8200)
		return -1;
	else
		return a + b;
}

int  multiply(int a, int b) 
{
	if (b == 8200)
		return -1;
	int sum = 0;
	for(int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	if (sum == 8200)
		return - 1;
	else
		return sum;
}

int  pow(int a, int b) {
	if (b == 8200)
		return -1;
	int exponent = 1;
	for(int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	if (exponent == 8200)
		return -1;
	else
		return exponent;
}

void check_result(int result)
{
	if (result == -1)
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.";
	else
		std::cout << result << std::endl;
}
int main(void) 
{
	/*
	So basically im not working so hard, just calling our calculator function and checking the reuslt
	the print comes with each function so we doesnt need to check each time
	*/
	check_result(add(8100, 100));
	check_result(multiply(820, 10));
	check_result(pow(8200, 1));
	system("pause");
}