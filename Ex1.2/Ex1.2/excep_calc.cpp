#include <iostream>
int add(int a, int b) {
	if (a == 8200 || b == 8200 || a + b == 8200)
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."));
	else
		return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  if (b == 8200)
	  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."));
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
  };
  if (sum == 8200)
	  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."));
  else
		return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  if (b == 8200)
	  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."));
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  if (exponent == 8200)
	  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."));
  else
	return exponent;
}

int main(void) {
	try
	{
		std::cout << add(8000, 200) << std::endl;
	}
	catch (const std::string& ErrorS)
	{
		std::cout << ErrorS.c_str() << std::endl;
	}
	try
	{
		std::cout << multiply(1640, 5) << std::endl;
	}
	catch (const std::string& ErrorS)
	{
		std::cout << ErrorS.c_str() << std::endl;
	}
	try
	{
		std::cout << pow(5, 5) << std::endl;
	}
	catch (const std::string& ErrorS)
	{
		std::cout << ErrorS.c_str() << std::endl;
	}
	system("pause");
}